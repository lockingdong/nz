<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb px-0">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item active" aria-current="page">Cookie政策</li>
      </ol>
    </nav>
    <h2 class="page-title privacy-title">Cookie政策</h2>
    <article class="privacy-content">
      <p>日進電線股份有限公司創立於1983年，耐熱電線電纜起步，目前日進電線已是台灣電線電纜及特殊線材產業領導廠商，同時成功跨足綠能光電、成為國際化企業。</p>
      <br>
      <p>日進電線所生產的矽膠線、補償導線、PVC線、不銹鋼線材，廣泛運用於電力傳輸、電信網路、交通運輸、工業生產等基礎建設。旗下核心事業中，電線電纜事業包含矽膠耐熱電線、補償導線、PVC照射線等電線電纜。電力電纜與通信線纜產品線完整，深耕台灣電力和電信需求。</p>
      <br>
      <p>日進電線擁有完整的產品系列，通過數種國際安規認可產品。以最熱忱的服務態度，不斷精進品質，開發新產品，和客戶共同發展、共同成長。由于您持續的支持與愛護，以前瞻性的佈局策略追求企業創新成長，進行自主技術之研究，依據市場及客戶需求開發新產品與新業務。日進電線秉持一貫對品質的嚴謹要求，以及快速整合的服務，成為客戶的最佳伙伴，在兩岸經濟發展的重要里程中，扮演關鍵的參與和推動角色，未來日進電線將在卓越製造技術與多樣化客戶基礎下持續深耕，同時積極掌握產業新興發展機會，創造企業發展的新里程。</p>
    </article>
  </section>

  <footer class="">
    <div class="cst-container">
      <div class="row">
        <div class="col-sm-8 col-12 mx-auto">
          <div class="row">
            <div class="col-md-6 col-12 my-3">
              <a class="mr-4" href="https://www.google.com.tw/maps/place/%E6%97%A5%E9%80%B2%E9%9B%BB%E7%B7%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8/@25.0587124,121.4718179,17z/data=!3m1!4b1!4m5!3m4!1s0x3442a7e768a1d123:0x1b3b1220987a8188!8m2!3d25.0587076!4d121.4740119?hl=zh-TW">
                <svg ersion="1.1" id="圖層_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 17.8 25.6" style="enable-background:new 0 0 17.8 25.6;" xml:space="preserve" >
                  <path class="st0" class="st0" d="M8.9,0C4,0,0,4,0,8.9c0,2.3,1.5,5.8,4.3,10.4c2.1,3.4,4.3,6.1,4.3,6.1c0.1,0.1,0.2,0.1,0.3,0.1
                  c0,0,0,0,0,0c0.1,0,0.2-0.1,0.3-0.2c0,0,2.2-2.8,4.3-6.2c2.9-4.6,4.3-8.1,4.3-10.4C17.8,4,13.8,0,8.9,0z M8.9,13.2
                  c-2.4,0-4.3-1.9-4.3-4.3s1.9-4.3,4.3-4.3s4.3,1.9,4.3,4.3S11.3,13.2,8.9,13.2z"/>
                </svg>
              </a>
              <a class="mr-4" href="">
                <svg version="1.1" id="圖層_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;margin-top: -2px" xml:space="preserve" >
                  <path class="st0" d="M25.2,4.4V24c0,1.5-1.2,2.7-2.7,2.7h-4.8v-9.9h3.4l0.5-3.8h-3.9v-2.4c0-1.1,0.3-1.8,1.9-1.8h2V5.4
                  c-0.3,0-1.5-0.2-2.9-0.2c-2.9,0-4.9,1.8-4.9,5V13h-3.4v3.8h3.4v9.9h-11c-1.5,0-2.7-1.2-2.7-2.7V4.4c0-1.5,1.2-2.7,2.7-2.7h19.7
                    C24,1.7,25.2,2.9,25.2,4.4z"/>
                </svg>
              </a>
              <a href="">
                <svg version="1.1" id="圖層_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
                <path class="st0" d="M15.2,9.6v4c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.1,0-0.1-0.1l-1.8-2.5v2.4c0,0.1-0.1,0.2-0.2,0.2h-0.6
                c-0.1,0-0.2-0.1-0.2-0.2v-4c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.1,0,0.1,0.1l1.8,2.5V9.6c0-0.1,0.1-0.2,0.2-0.2H15
                C15.1,9.5,15.2,9.5,15.2,9.6L15.2,9.6z M10.6,9.5H10c-0.1,0-0.2,0.1-0.2,0.2v4c0,0.1,0.1,0.2,0.2,0.2h0.6c0.1,0,0.2-0.1,0.2-0.2v-4
                C10.8,9.5,10.7,9.5,10.6,9.5L10.6,9.5z M9.1,12.8H7.4V9.6c0-0.1-0.1-0.2-0.2-0.2H6.5c-0.1,0-0.2,0.1-0.2,0.2v4c0,0.1,0,0.1,0.1,0.1
                c0,0,0.1,0.1,0.1,0.1h2.6c0.1,0,0.2-0.1,0.2-0.2V13C9.3,12.9,9.2,12.8,9.1,12.8L9.1,12.8z M18.6,9.5H16c-0.1,0-0.2,0.1-0.2,0.2v4
                c0,0.1,0.1,0.2,0.2,0.2h2.6c0.1,0,0.2-0.1,0.2-0.2V13c0-0.1-0.1-0.2-0.2-0.2h-1.7v-0.7h1.7c0.1,0,0.2-0.1,0.2-0.2v-0.6
                c0-0.1-0.1-0.2-0.2-0.2h-1.7v-0.7h1.7c0.1,0,0.2-0.1,0.2-0.2V9.6C18.8,9.5,18.7,9.5,18.6,9.5L18.6,9.5z M25.1,4.6v16
                c0,2.5-2.1,4.5-4.6,4.5h-16C2,25.1,0,23,0,20.5v-16C0,2,2.1,0,4.6,0h16C23,0,25.1,2.1,25.1,4.6z M21.6,11.4c0-4.1-4.1-7.4-9.1-7.4
                s-9.1,3.3-9.1,7.4c0,3.7,3.2,6.7,7.6,7.3c1.1,0.2,0.9,0.6,0.7,2.1c0,0.2-0.2,0.9,0.8,0.5c1-0.4,5.3-3.1,7.2-5.3
                C21,14.5,21.6,13.1,21.6,11.4z"/>
                </svg>
              </a>
            </div>
            <div class="col-md-6 col-12 my-3 text-md-left text-center">
              <a class="mr-3 footer-link" href="">隱私權聲明</a>
              <a class="footer-link" href="">Cookie政策</a>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-introduction mx-auto">
        <p class="d-md-inline-block d-block mr-2">服務時間：AM:08:30~PM06:00</p>
        <p class="d-md-inline-block d-block mr-2">電話:02-2999-9181</p>
        <p class="d-md-inline-block d-block mr-2">傳真:02-2999-9771</p>
        <p><a class="footer-link font-weight-light" href="https://www.google.com.tw/maps/place/%E6%97%A5%E9%80%B2%E9%9B%BB%E7%B7%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8/@25.0587124,121.4718179,17z/data=!3m1!4b1!4m5!3m4!1s0x3442a7e768a1d123:0x1b3b1220987a8188!8m2!3d25.0587076!4d121.4740119?hl=zh-TW">台灣新北市三重區光復路二段87巷10-12號</a></p>
        <p class="my-4">© Moveon Design. <span class="d-inline-block">All right reserved 2018.</span></p>
      </div>
    </div>
  </footer>
  <div class="full-background" style="background-image: url('./src/dist/image/privacy-bg.jpg')"></div>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
  <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="./src/dist/js/all.min.js"></script>
</body>
</html>
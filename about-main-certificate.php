<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center">安規認證<span></span></h1>
  </section>
  <section class="container about-main-certificate-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb p-0 mb-20 mt-20">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item active">安規認證</li>
      </ol>
    </nav>
    <section class="container" style="width:100%">
      <div class="row mb-10">
        <a class="col-12 about-certificate-bg about-page-bg-title" href="about-certificate.php" style="background-image: url('./src/dist/image/about/about-certificate-bg-1.jpg')">
          <div class="about-page-bg-info about-page-bg-info-black">
            <p class="about-content-title">規格認證</p>
            <p class="about-seemore">查看詳情 ></p>
          </div>
        </a>
      </div>
      <div class="row mb-10">
        <a class="col-12 about-certificate-bg about-page-bg-title" href="about-certificate.php" style="background-image: url('./src/dist/image/about/about-certificate-bg-2.jpg')">
          <div class="about-page-bg-info">
            <p class="about-content-title">品質認證</p>
            <p class="about-seemore">查看詳情 ></p>
          </div>
        </a>
      </div>
      <div class="row mb-10">
        <a class="col-12 about-certificate-bg about-page-bg-title" href="about-certificate.php" style="background-image: url('./src/dist/image/about/about-certificate-bg-3.jpg')">
          <div class="about-page-bg-info about-page-bg-info-black">
            <p class="about-content-title">綠色認證</p>
            <p class="about-seemore">查看詳情 ></p>
          </div>
        </a>
      </div>
     </section>
  </section>
  
  <?php include './component/footer.php' ?>
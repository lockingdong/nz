<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="./src/dist/css/all.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" >
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css">
  <title>Document</title>

<script type="text/javascript">

var imf = function () {
	var lf = 0;
	var instances = [];
	//定义一个通过class获得元素的方法
	function getElementsByClass (object, tag, className) {
		var o = object.getElementsByTagName(tag);
		for ( var i = 0, n = o.length, ret = []; i < n; i++)
			if (o[i].className == className) ret.push(o[i]);
		if (ret.length == 1) ret = ret[0];
		return ret;
	}
	function addEvent (o, e, f) {
		if (window.addEventListener) o.addEventListener(e, f, false);
		else if (window.attachEvent) r = o.attachEvent('on' + e, f);
	}
	function createReflexion (cont, img) {
		var flx = false;
		// if (document.createElement("canvas").getContext) {
		// 	flx = document.createElement("canvas");
		// 	flx.width = img.width;
		// 	flx.height = img.height;
		// 	var context = flx.getContext("2d");
		// 	context.translate(0, img.height);
		// 	context.scale(1, -1);
		// 	context.drawImage(img, 0, 0, img.width, img.height);
		// 	context.globalCompositeOperation = "destination-out";
		// 	var gradient = context.createLinearGradient(0, 0, 0, img.height * 2);
		// 	gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
		// 	gradient.addColorStop(0, "rgba(255, 255, 255, 1)");
		// 	context.fillStyle = gradient;
		// 	context.fillRect(0, 0, img.width, img.height * 2);
		// } else {
		// 	/* ---- DXImageTransform ---- */
		// 	flx     = document.createElement('img');
		// 	flx.src = img.src;
		// 	flx.style.filter = 'flipv progid:DXImageTransform.Microsoft.Alpha(' +
		// 	                   'opacity=50, style=1, finishOpacity=0, startx=0, starty=0, finishx=0, finishy=' +
		// 					   (img.height * .25) + ')';
		// }
		// /* ---- insert Reflexion ---- */
		// flx.style.position = 'absolute';
		// flx.style.left     = '-1000px';
		// cont.appendChild(flx);
		return flx;
	}
	/* //////////// ==== ImageFlow Constructor ==== //////////// */
	function ImageFlow(oCont, size, zoom, border) {
		this.diapos     = [];
		this.scr        = false;
		this.size       = size;
		this.zoom       = zoom;
		this.bdw        = border;
		this.oCont      = oCont;
		this.oc         = document.getElementById(oCont);
		this.scrollbar  = getElementsByClass(this.oc,   'div', 'scrollbar');
		this.text       = getElementsByClass(this.oc,   'div', 'text');
		this.title      = getElementsByClass(this.text, 'div', 'title');
		this.legend     = getElementsByClass(this.text, 'div', 'legend');
		this.bar        = getElementsByClass(this.oc,   'img', 'bar');
		this.arL        = getElementsByClass(this.oc,   'span', 'arrow-left');
		this.arR        = getElementsByClass(this.oc,   'span', 'arrow-right');
		this.bw         = this.bar.width;
		this.alw        = this.arL.width - 5;
		this.arw        = this.arR.width - 5;
		this.bar.parent = this.oc.parent  = this;
		this.arL.parent = this.arR.parent = this;
		this.view       = this.back       = 3;
		this.resize();
		this.oc.onselectstart = function () { return false; }
		/* ---- create images ---- */
		var img   = getElementsByClass(this.oc, 'div', 'bank').getElementsByTagName('a');
		this.NF = img.length;
		for (var i = 0, o; o = img[i]; i++) {
			this.diapos[i] = new Diapo(this, i,
										o.rel,
										o.title || '- ' + i + ' -',
										o.innerHTML || o.rel,
										o.href || '',
										o.target || '_self'
			);
		}
		/* ==== add mouse wheel events ==== */
		if (window.addEventListener)
			this.oc.addEventListener('DOMMouseScroll', function(e) {
				this.parent.scroll(-e.detail);
			}, false);
		else this.oc.onmousewheel = function () {
			this.parent.scroll(event.wheelDelta);
		}
		/* ==== scrollbar drag N drop ==== */
		this.bar.onmousedown = function (e) {
			if (!e) e = window.event;
			var scl = e.screenX - this.offsetLeft;
			var self = this.parent;
			/* ---- move bar ---- */
			this.parent.oc.onmousemove = function (e) {
				if (!e) e = window.event;
				self.bar.style.left = Math.round(Math.min((self.ws - self.arw - self.bw), Math.max(self.alw, e.screenX - scl))) + 'px';
				self.view = Math.round(((e.screenX - scl) ) / (self.ws - self.alw - self.arw - self.bw) * self.NF);
				if (self.view != self.back) self.calc();
				return false;
			}
			/* ---- release scrollbar ---- */
			this.parent.oc.onmouseup = function (e) {
				self.oc.onmousemove = null;
				return false;
			}
			return false;
		}
		/* ==== right arrow ==== */
		this.arR.onclick = this.arR.ondblclick = function () {
			if (this.parent.view < this.parent.NF - 1)
				this.parent.calc(1);
		}
		/* ==== Left arrow ==== */
		this.arL.onclick = this.arL.ondblclick = function () {
			if (this.parent.view > 0)
				this.parent.calc(-1);
		}
	}
	/* //////////// ==== ImageFlow prototype ==== //////////// */
	ImageFlow.prototype = {
		/* ==== targets ==== */
		calc : function (inc) {
			if (inc) this.view += inc;
			var tw = 0;
			var lw = 0;
			var o = this.diapos[this.view];
			if (o && o.loaded) {
				/* ---- reset ---- */
				var ob = this.diapos[this.back];
				if (ob && ob != o) {
					ob.img.className = 'diapo';
					ob.z1 = 1;
				}
				/* ---- update legend ---- */
				this.title.replaceChild(document.createTextNode(o.title), this.title.firstChild);
				this.legend.replaceChild(document.createTextNode(o.text), this.legend.firstChild);
				/* ---- update hyperlink ---- */
				if (o.url) {
					o.img.className = 'diapo link';
					window.status = 'hyperlink: ' + o.url;
				} else {
					o.img.className = 'diapo';
					window.status = '';
				}
				/* ---- calculate target sizes & positions ---- */
				o.w1 = Math.min(o.iw, this.wh * .5) * o.z1;
				var x0 = o.x1 = (this.wh * .5) - (o.w1 * .5);
				var x = x0 + o.w1 + this.bdw;
				for (var i = this.view + 1, o; o = this.diapos[i]; i++) {
					if (o.loaded) {
						o.x1 = x;
						o.w1 = (this.ht / o.r) * this.size;
						x   += o.w1 + this.bdw;
						tw  += o.w1 + this.bdw;
					}
				}
				x = x0 - this.bdw;
				for (var i = this.view - 1, o; o = this.diapos[i]; i--) {
					if (o.loaded) {
						o.w1 = (this.ht / o.r) * this.size;
						o.x1 = x - o.w1;
						x   -= o.w1 + this.bdw;
						tw  += o.w1 + this.bdw;
						lw  += o.w1 + this.bdw;
					}
				}
				/* ---- move scrollbar ---- */
				if (!this.scr && tw) {
					var r = (this.ws - this.alw - this.arw - this.bw) / tw;
					// this.bar.style.left = Math.round(this.alw + lw * r) + 'px';
				}
				/* ---- save preview view ---- */
				this.back = this.view;
			}
		},
		/* ==== mousewheel scrolling ==== */
		scroll : function (sc) {
			if (sc < 0) {
				if (this.view < this.NF - 1) this.calc(1);
			} else {
				if (this.view > 0) this.calc(-1);
			}
		},
		/* ==== resize  ==== */
		resize : function () {
			this.wh = this.oc.clientWidth;
			this.ht = this.oc.clientHeight;
			this.ws = this.scrollbar.offsetWidth;
			this.calc();
			this.run(true);
		},
		/* ==== move all images  ==== */
		run : function (res) {
			var i = this.NF;
			while (i--) this.diapos[i].move(res);
		}
	}
	/* //////////// ==== Diapo Constructor ==== //////////// */
	Diapo = function (parent, N, src, title, text, url, target) {
		this.parent        = parent;
		this.loaded        = false;
		this.title         = title;
		this.text          = text;
		this.url           = url;
		this.target        = target;
		this.N             = N;
		this.img           = document.createElement('img');
		this.img.src       = src;
		this.img.parent    = this;
		this.img.className = 'diapo';
		this.x0            = this.parent.oc.clientWidth;
		this.x1            = this.x0;
		this.w0            = 0;
		this.w1            = 0;
		this.z1            = 1;
		this.img.parent    = this;
		this.img.onclick   = function() { this.parent.click(); }
		this.parent.oc.appendChild(this.img);
		/* ---- display external link ---- */
		if (url) {
			this.img.onmouseover = function () { this.className = 'diapo link';	}
			this.img.onmouseout  = function () { this.className = 'diapo'; }
		}
	}
	/* //////////// ==== Diapo prototype ==== //////////// */
	Diapo.prototype = {
		/* ==== HTML rendering ==== */
		move : function (res) {
			if (this.loaded) {
				var sx = this.x1 - this.x0;
				var sw = this.w1 - this.w0;
				if (Math.abs(sx) > 2 || Math.abs(sw) > 2 || res) {
					/* ---- paint only when moving ---- */
					this.x0 += sx * .1;
					this.w0 += sw * .1;
					if (this.x0 < this.parent.wh && this.x0 + this.w0 > 0) {
						/* ---- paint only visible images ---- */
						this.visible = true;
						var o = this.img.style;
						var h = this.w0 * this.r;
						/* ---- diapo ---- */
						o.left   = Math.round(this.x0) + 'px';
						o.bottom = Math.floor(this.parent.ht * .25) + 'px';
						o.width  = Math.round(this.w0) + 'px';
						o.height = Math.round(h) + 'px';
						/* ---- reflexion ---- */
						if (this.flx) {
							var o = this.flx.style;
							o.left   = Math.round(this.x0) + 'px';
							o.top    = Math.ceil(this.parent.ht * .75 + 1) + 'px';
							o.width  = Math.round(this.w0) + 'px';
							o.height = Math.round(h) + 'px';
						}
					} else {
						/* ---- disable invisible images ---- */
						if (this.visible) {
							this.visible = false;
							this.img.style.width = '0px';
							if (this.flx) this.flx.style.width = '0px';
						}
					}
				}
			} else {
				/* ==== image onload ==== */
				if (this.img.complete && this.img.width) {
					/* ---- get size image ---- */
					this.iw     = this.img.width + 30;
					this.ih     = this.img.height + 30;
					this.r      = this.ih / this.iw;
					this.loaded = true;
					/* ---- create reflexion ---- */
					this.flx    = createReflexion(this.parent.oc, this.img);
					if (this.parent.view < 0) this.parent.view = this.N;
					this.parent.calc();
				}
			}
		},
		/* ==== diapo onclick ==== */
		click : function () {
			if (this.parent.view == this.N) {
				/* ---- click on zoomed diapo ---- */
				if (this.url) {
					/* ---- open hyperlink ---- */
					window.open(this.url, this.target);
				} else {
					/* ---- zoom in/out ---- */
					this.z1 = this.z1 == 1 ? this.parent.zoom : 1;
					this.parent.calc();
				}
			} else {
				/* ---- select diapo ---- */
				this.parent.view = this.N;
				this.parent.calc();
			}
			return false;
		}
	}
	/* //////////// ==== public methods ==== //////////// */
	return {
		/* ==== initialize script ==== */
		create : function (div, size, zoom, border) {
			/* ---- instanciate imageFlow ---- */
			var load = function () {
				var loaded = false;
				var i = instances.length;
				while (i--) if (instances[i].oCont == div) loaded = true;
				if (!loaded) {
					/* ---- push new imageFlow instance ---- */
					instances.push(
						new ImageFlow(div, size, zoom, border)
					);
					/* ---- init script (once) ---- */
					if (!imf.initialized) {
						imf.initialized = true;
						/* ---- window resize event ---- */
						addEvent(window, 'resize', function () {
							var i = instances.length;
							while (i--) instances[i].resize();
						});
						/* ---- stop drag N drop ---- */
						addEvent(document.getElementById(div), 'mouseout', function (e) {
							if (!e) e = window.event;
							var tg = e.relatedTarget || e.toElement;
							if (tg && tg.tagName == 'HTML') {
								var i = instances.length;
								while (i--) instances[i].oc.onmousemove = null;
							}
							return false;
						});
						/* ---- set interval loop ---- */
						setInterval(function () {
							var i = instances.length;
							while (i--) instances[i].run();
						}, 16);
					}
				}
			}
			/* ---- window onload event ---- */
			addEvent(window, 'load', function () { load(); }); 
		}
	}
}();

/* ==== create imageFlow ==== */
//          div ID    , size, zoom, border
imf.create("imageFlow", 0.15, 2, 12);
imf.create("imageFlow2", 0.15, 2, 12);
</script>
</head>
<body >
  <?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron"> 
    <h1 class="text-center"><span>日進電線 - </span>設備技術</h1>
  </section>
  <div class="about-capability">
    <section class="container ">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 mb-0">
          <li class="breadcrumb-item"><a href="#">首頁</a></li>
          <li class="breadcrumb-item"><a href="#">日進電線</a></li>
          <li class="breadcrumb-item active">設備技術</li>
        </ol>
      </nav>
      <h2 class="page-title">製程能力</h2>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">單芯纜線</span>
        <p class="mb-0">線種包括電子線、機具用線、耐高溫線、耐高壓線、多層絕緣線、加熱線、...等</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">多芯複合線纜</span>
        <p class="mb-0">線種包括防盜線、視聽用線、資料傳輸用線、醫療線、儀器用線、網路線、電話線、USB傳輸線、機器手臂用線、熱補償導線、各式UL線種、各式捲線、超軟線...等</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">導體</span>
        <p class="mb-0">除一般裸銅、鍍錫、鍍鎳或鍍銀銅單線或絞線外，尚可提供銅包鋼、鍍銀銅包鋼、鍍鋅鋼線、鎘銅(鍍錫或鍍銀)、鎳烙合金、錫銅合金、銅鐵合金、銀線、金線、鍍金線...等各種特殊材料</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">遮蔽層</span>
        <p class="mb-0">可提供各種型態之遮蔽設計，包括纏繞、編織鋁箔麥拉帶、麥拉帶、棉紙，e-PTFE...等</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">絕緣及外被</span>
        <p class="mb-0">可供應用之材料廣泛，函括PVC、PP、PE、PU、HYTREL、TPEE、NYLON、PFA、FEP、ETFE、PVDF、TPE、TPR、TPV、TPO、SILICONE、XLPE、導電塑膠(PE，PVC)、低煙無鹵...等</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">加強</span>
        <p class="mb-0">Kevlar, 尼龍線, 棉線, Nomex, Technora, 玻纖...</p>
      </div>
      <div class="my-20 ">
        <span class="badge badge-capability mb-15">客製化服務</span>
        <p class="mb-0">各類線種之客製化 OEM & ODM 服務，為您量身訂做符合您特殊需求之線材。</p>
      </div>
        
    </section>
  </div>
  <!-- <div class="marquee marquee-capability">
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-1.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-2.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-3.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-4.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-1.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-2.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/about-cap-3.jpg');"></div>
    </div>
  </div> -->

  <div class="about-capability-bg" style="background-image: url('./src/dist/image/about-cap-bg1.jpg');" >
    <h2 class="page-title">生產設備</h2>
    <div id="imageFlow2">
      <div class="bank">
        <a rel="./src/dist/image/about/about-cap-slick-1.jpg" title="動力放線架" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-2.jpg" title="溫度控制箱" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-3.jpg" title="自動填料機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-4.jpg" title="押出機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-5.jpg" title="引取機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-6.jpg" title="搖盤機" href="javascript:;"> </a> 
      </div>
      <div class="text">
        <div class="title">
          Loading
        </div>
        <div class="legend">
          Please wait...
        </div>
      </div>
      <div class="scrollbar">
        <span class="arrow-left">
          <i class="fas fa-angle-left"></i>
        </span>
        <span class="arrow-right">
          <i class="fas fa-angle-right"></i>
        </span>
      </div>
    </div>
  </div>

  <div class="about-capability-bg" style="background-image: url('./src/dist/image/about-cap-bg1.jpg');" >
    <h2 class="page-title">檢測儀器</h2>
    <div id="imageFlow">
      <div class="bank">
        <a rel="./src/dist/image/about/about-cap-slick-2.jpg" title="動力放線架" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-1.jpg" title="溫度控制箱" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-4.jpg" title="自動填料機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-5.jpg" title="押出機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-3.jpg" title="引取機" href="javascript:;"> </a>
        <a rel="./src/dist/image/about/about-cap-slick-6.jpg" title="搖盤機" href="javascript:;"> </a> 
      </div>
        <div class="text">
          <div class="title">
            Loading
          </div>
          <div class="legend">
            Please wait...
          </div>
        </div>
        <div class="scrollbar">
          <span class="arrow-left">
            <i class="fas fa-angle-left"></i>
          </span>
          <span class="arrow-right">
            <i class="fas fa-angle-right"></i>
          </span>
        </div>
    </div>
  </div>
  <?php include './component/footer.php' ?>
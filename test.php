<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body, html {
            padding: 0;
            margin: 0;
        }
        .wrap {
            position: relative;
        }
        .one {
            width: 100%;
            z-index: 0;
        }
        .two {
            position: absolute;
            top: 20%;
            left: 10%;
            width: 15%;
            z-index: 100;
        }

    </style>
</head>
<body>
    <div class="wrap">
        <img class="one" src="1.jpg" alt="">
        <img class="two" src="left-1.png" alt="">

    </div>
    
    
</body>
</html>
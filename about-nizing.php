<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
<style>
html, body {
  overflow-x: hidden !important;
}
</style>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center">日進電線</h1>
  </section>
  <section class="container breadcrumb-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb px-0 mb-20">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item active">日進電線</li>
      </ol>
    </nav>
  </section> 
  <div class="container about-nizing">
    <a class="row mb-10" href="company-introduction.php" >
      <div class="col-lg-8 col-sm-7 about-nizing-bg" style="background-image: url('./src/dist/image/about/about-nizing-01.jpg')"></div>
      <div class="col-lg-4 col-sm-5 about-nizing-info">
        <div class="about-nizing-info-content my-10">
          <p class="about-content-title">公司簡介</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10" href="company-cultrue.php" >
      <div class="col-lg-8 col-sm-7 about-nizing-bg" style="background-image: url('./src/dist/image/about/about-nizing-02.jpg')"></div>
      <div class="col-lg-4 col-sm-5 about-nizing-info">
        <div class="about-nizing-info-content my-10">
          <p class="about-content-title">核心文化</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10" href="page-history.php" >
      <div class="col-lg-8 col-sm-7 about-nizing-bg" style="background-image: url('./src/dist/image/about/about-nizing-03.jpg')"></div>
      <div class="col-lg-4 col-sm-5 about-nizing-info">
        <div class="about-nizing-info-content my-10">
          <p class="about-content-title">歷史紀錄</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10" href="about-capability.php" >
      <div class="col-lg-8 col-sm-7 about-nizing-bg" style="background-image: url('./src/dist/image/about/about-nizing-04.jpg')"></div>
      <div class="col-lg-4 col-sm-5 about-nizing-info">
        <div class="about-nizing-info-content my-10">
          <p class="about-content-title">設備技術</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10" href="company-statement.php" >
      <div class="col-lg-8 col-sm-7 about-nizing-bg" style="background-image: url('./src/dist/image/about/about-nizing-05.jpg')"></div>
      <div class="col-lg-4 col-sm-5 about-nizing-info">
        <div class="about-nizing-info-content my-10">
          <p class="about-content-title">無衝突金屬聲明</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
  </div>
<?php include './component/footer.php' ?>
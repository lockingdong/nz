<header>
    <div class="navbar-info">
      <nav aria-label="breadcrumb" class="d-inline-block ml-auto mr-5">
        <ol class="breadcrumb mb-0">
          <li class="breadcrumb-item active">繁體中文</li>
          <li class="breadcrumb-item navbar-info-link" aria-current="page"><a href="#">English</a></li>
        </ol>
      </nav> 
      <a href="#" class="navbar-info-link mr-5">職員專區</a>
      <a href="#" class="navbar-info-link mr-5">線上估價</a>
      <a href="#" class="navbar-info-link mr-5">徵才資訊</a>
      <form action="" class=" navbar-search">
        <input type="text" class="navbar-search-text">
        <button type="submit" class="navbar-search-btn"><i class="fas fa-search"></i></button>
      </form>       
    </div>
    <nav class="navbar navbar-expand-xl navbar-light ">
      <a class="navbar-brand" href="index.php">
        <img src="./src/dist/image/nizing-logo.svg" alt="logo" >
        <!-- <img src="./src/dist/image/links-01.jpg" alt=""> -->
      </a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar top-bar"></span>
        <span class="icon-bar middle-bar"></span>
        <span class="icon-bar bottom-bar"></span>
        <span class="icon-bar"></span>
        <span class="sr-only">Toggle navigation</span>
    	</button>
      <!-- <div class="navbar-hamburger-wrapper">
        <button class="navbar-toggler special-button p-0 border-0" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 150 150"  xml:space="preserve">
            <path class="st0" d="M41,78h71c0,0,29.7-8,16-36c-1.6-3.3-19.3-30-52-30c-36,0-64,29-64,65c0,29,26,59,61.6,59
            c41.4,0,62.6-29,62.6-59c0-37.7-19.2-49.1-26.3-54"
                stroke-linejoin="round" 
                stroke-linecap="round"/>
          </svg>
          <div class="top"></div>
          <div class="bottom"></div>
        </button>
      </div> -->
      <div class="collapse navbar-collapse navbar-menu" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item navbar-info-link-mobile">
            <form action="" class=" navbar-search">
              <input type="text" class="navbar-search-text">
              <button type="submit" class="navbar-search-btn"><i class="fas fa-search"></i></button>
            </form>  
          </li>
          <li class="nav-item dropdown navbar-info-link-mobile ">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:;">語系切換</a>
            <div class="dropdown-menu" >
              <div class="cst-container">
                <div class="row">
                  <a class="col-xl-4  col-12 mb-2" href="#">
                    <span class="dropdown-item" >繁體中文</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="#">
                    <span class="dropdown-item" >English</span>
                  </a>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown" >
            <a class="nav-link dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">日進電線</a>
            <div class="dropdown-menu" >
              <div class="cst-container">
                <h3 class="mb-15"><a href="about-nizing.php"> 日進電線 </a><span class="close">X</span></h3>
                <div class="row">
                  <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-nizing.php">
                    <span class="dropdown-item" >日進電線</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="company-introduction.php">
                    <span class="dropdown-item" >公司簡介</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="company-cultrue.php">
                    <span class="dropdown-item" >核心文化</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="page-history.php">
                    <span class="dropdown-item" >歷史紀錄</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-capability.php">
                    <span class="dropdown-item" >設備技術</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="company-statement.php">
                    <span class="dropdown-item" >無衝突金屬聲明</span>
                  </a>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown ">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >產品資訊</a>
            <div class="dropdown-menu" >
              <div class="cst-container">
                <h3 class="mb-15"><a href="about-product.php"> 產品資訊</a><span class="close">X</span></h3>
                <div class="row">
                  <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-product.php">
                    <span class="dropdown-item" >產品資訊</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >一般電線電纜-PVC</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >鐵氟龍線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >高溫線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >矽膠耐熱線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >交連照射線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >套管系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >補償導線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >發熱線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >汽車花線系列</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >特殊訂製線材</span>
                  </a>
                  <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                    <span class="dropdown-item" >軍規線系列</span>
                  </a>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >應用產業</a>
              <div class="dropdown-menu" >
                <div class="cst-container">
                  <h3 class="mb-15"><a href="about-application.php"> 應用產業 </a> <span class="close">X</span></h3>
                  <div class="row">
                    <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-application.php">
                      <span class="dropdown-item" >應用產業</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >車用元件</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >雲端系統</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >機器手臂</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >加熱系統</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >醫療科技</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >LED</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >溫控系統</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >建築材料</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >太陽能</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >鋼鐵工業</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-prod-item.php">
                      <span class="dropdown-item" >其他應用</span>
                    </a>
                  </div>
                </div>
              </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >材料特性</a>
              <div class="dropdown-menu" >
                <div class="cst-container">
                  <h3 class="mb-15"><a href="about-material.php"> 材料特性</a> <span class="close">X</span></h3>
                  <div class="row">
                    <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-material.php">
                      <span class="dropdown-item" >材料特性</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-material.php">
                      <span class="dropdown-item" >塑料</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-material.php">
                      <span class="dropdown-item" >矽膠</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-material.php">
                      <span class="dropdown-item" >鐵氟龍</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-material.php">
                      <span class="dropdown-item" >纏繞編織材料</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-material.php">
                      <span class="dropdown-item" >導體</span>
                    </a>
                  </div>
                </div>
              </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >合金導體</a>
              <div class="dropdown-menu" >
                <div class="cst-container">
                  <h3 class="mb-15"><a href="about-alloy.php"> 合金導體</a> <span class="close">X</span></h3>
                  <div class="row">
                    <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-alloy.php">
                      <span class="dropdown-item" >合金導體</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >純銅類</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >銀銅合金類</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >錫銅合金類</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >鎳銅合金類</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >純銀及銀合金</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="page-alloy.php">
                      <span class="dropdown-item" >銅包鋼類</span>
                    </a>
                  </div>
                </div>
              </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >安規認證</a>
              <div class="dropdown-menu" >
                <div class="cst-container">
                  <h3 class="mb-15"><a href="about-main-certificate.php"> 安規認證 </a><span class="close">X</span></h3>
                  <div class="row">
                    <a class="col-xl-4  col-12 mb-2 navbar-info-link-mobile" href="about-main-certificate.php">
                      <span class="dropdown-item" >安規認證</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-certificate.php">
                      <span class="dropdown-item" >規格認證</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-certificate.php">
                      <span class="dropdown-item" >品質認證</span>
                    </a>
                    <a class="col-xl-4  col-12 mb-2" href="about-certificate.php">
                      <span class="dropdown-item" >綠色認證</span>
                    </a>
                  </div>
                </div>
              </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page-ulnumber.php">UL編號</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact-us.php">聯絡我們</a>
          </li>
          <li class="nav-item navbar-info-link-mobile">
            <a class="nav-link " href="#">職員專區</a>
          </li>
          <li class="nav-item navbar-info-link-mobile">
            <a class="nav-link " href="#">線上估價</a>
          </li>
          <li class="nav-item navbar-info-link-mobile">
            <a class="nav-link " href="#">徵才資訊</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <div class="header-mt"></div>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="./src/dist/css/all.min.css">
  <link rel="stylesheet" href="./src/dist/css/video.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <title>nizing</title>
</head>
<body>
    <button class="sound" onclick="toggleMute();">
      <i class="fas fa-volume-up" style="display:none;"></i>
      <i class="fas fa-volume-off"></i>
    </button>
    <div class="video-container">
      
      <!-- <div class="video-holder"></div> -->
      <!-- <video id="home-video" autoplay loop muted playsinline src="images/video.mp4"></video> -->
      <div class="counter-area">
        <div class="preload-counter">1%</div>
        <div class="p-bar"></div>
      </div>
      <div class="flag-area">
        <div class="row1">
          <a href="index.php">
            <div class="flag">
              <div class="lan">Skip</div> 
            </div>
          </a>
        
        </div>
        
        <div class="cpr">
        
          <p>© 2019 Moveon-design 版權所有 All right reserved.</p>
        </div>
      </div>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/0.6.0/preloadjs.min.js'></script>
      <script src="./src/dist/js/video.js"></script>
    </div>
  </body>
  <script>
  function toggleMute() {

    var video=document.getElementById("home-video")

    if(video.muted){
      video.muted = false;
      $('.fa-volume-up').fadeIn();
      $('.fa-volume-off').css('display', 'none');
    } else {
      video.muted = true;
      
      $('.fa-volume-off').fadeIn();
      $('.fa-volume-up').css('display', 'none');
    }

  }
  
  
  
  // setTimeout(() => {
    
    
  // }, 500);
  
  
  </script>
</html>
<?php 
  $json = '[
    "./src/dist/image/ul.jpg",
    "./src/dist/image/pse.jpg",
    "./src/dist/image/vde.jpg",
    "./src/dist/image/ul.jpg",
    "./src/dist/image/pse.jpg",
    "./src/dist/image/vde.jpg"
  ]';
  $objs=json_decode($json);
?>

<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center">安規認證 <span> - 規格認證</span></h1>
  </section>
  <section class="container about-certificate-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item"><a href="#">安規認證</a></li>
        <li class="breadcrumb-item active" aria-current="page">規格認證</li>
      </ol>
    </nav>
    <section class="about-certificate ">
      <div class="container">
        <div class="certificate-row">
          <!-- repeat -->
          <?php foreach($objs as $obj): ?>
          <div class="img-wrap">
            <a href="page-certificate.php">
              <img src="<?php echo $obj;?>" alt="">
            </a>
          </div>
          <?php endforeach; ?>
          <!-- endrepeat -->
        </div>
      </div> 
    </section>
  </section>
  
  <?php include './component/footer.php' ?>
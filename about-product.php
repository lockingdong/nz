<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>

  <div class="about-product-breadcrumb">
    <section class="container breadcrumb-container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb pb-20">
          <li class="breadcrumb-item"><a href="#">首頁</a></li>
          <li class="breadcrumb-item active">產品資訊</li>
        </ol>
      </nav>
    </section> 
  </div>
 
  <div class="container about-product">
    <a class="row mb-10 about-product-bg" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-1.jpg')">
      <div class="col-lg-6 col-sm-7 col-12 about-product-info">
        <div class="about-product-info-content">
          <p class="about-content-title">一般電纜電纜 - PVC</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10 about-product-bg" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-2.jpg')">
      <div class="col-lg-6 col-sm-7 col-12 about-product-info">
        <div class="about-product-info-content">
          <p class="about-content-title">一般電纜電纜 - PVC</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10 about-product-bg" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-3.jpg')">
      <div class="col-lg-6 col-sm-7 col-12 about-product-info about-product-info-white">
        <div class="about-product-info-content ">
          <p class="about-content-title">一般電纜電纜 - PVC</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <a class="row mb-10 about-product-bg" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-4.jpg')">
      <div class="col-lg-6 col-sm-7 col-12 about-product-info about-product-info-white">
        <div class="about-product-info-content ">
          <p class="about-content-title">一般電纜電纜 - PVC</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </div>
    </a> 
    <div class="container-fluid container-prod ">
      <div class="row  mb-10">
          <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-prod-bg-col-2 mb-10" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-L-1.jpg')">
            <div class="about-page-bg-info about-page-bg-info-black">
              <p class="about-content-title">一般電纜電纜 - PVC</p>
              <p class="about-seemore">查看詳情 ></p>
            </div>
          </a>
          <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-prod-bg-col-2 mb-10" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-L-2.jpg')">
            <div class="about-page-bg-info about-page-bg-info-black">
              <p class="about-content-title">一般電纜電纜 - PVC</p>
              <p class="about-seemore">查看詳情 ></p>
            </div>
          </a>
          <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-prod-bg-col-2 mb-10" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-L-3.jpg')">
            <div class="about-page-bg-info ">
              <p class="about-content-title">一般電纜電纜 - PVC</p>
              <p class="about-seemore">查看詳情 ></p>
            </div>
          </a>
          <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-prod-bg-col-2 mb-10" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-L-4.jpg')">
            <div class="about-page-bg-info ">
              <p class="about-content-title">一般電纜電纜 - PVC</p>
              <p class="about-seemore">查看詳情 ></p>
            </div>
          </a>
          <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-prod-bg-col-2 mb-10" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-product-L-1.jpg')">
            <div class="about-page-bg-info about-page-bg-info-black">
              <p class="about-content-title">一般電纜電纜 - PVC</p>
              <p class="about-seemore">查看詳情 ></p>
            </div>
          </a>
        </div> 
    </div>
    
  </div>
  <?php include './component/footer.php' ?>
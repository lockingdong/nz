<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron"> 
    <h1 class="text-center">合金導體 - 純銅</h1>
  </section>
  <section class="cst-container ">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb px-0 mb-0 pt-0">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item "><a href="#">合金導體</a></li>
        <li class="breadcrumb-item active">純銅</li>
      </ol>
    </nav>
  </section> 
  <section class="container page-material"> 
    <div class="page-proditem-info-area">
      <div class="page-proditem-info-title">
        <p class="mb-0">純銅</p>
      </div>
      <div class="page-proditem-info page-material-info ">
        <div>
            <b>產品 </b>	高導性無氧銅線(棒)、單方向結晶無氧銅線(棒) <br>
            <div class="row mx-0">
              <div ><b>承製線徑 </b></div>
              <div >(可電鍍銀、錫、鋅、鎳等)<br>
                    OFC：0.1mm-16mm <br>
                    其餘：0.1mm-8.0mm
              </div>
            </div>
        
            
            <b>特性 </b>具有『高導電性』、『耐熱性』、『高強度』及『耐彎曲性』 等特點。<br>
            <b>應用 </b>極細同軸電纜、耐熱電線電纜、醫療用電線電纜、音響用電線電纜、小型振動線圈、聲波線圈、話筒線圈、小型馬達、NINTENDO電纜等。
        </div>
      </div>
    </div>
    <div class="page-material-table">          
      <table class="table">
        <thead>
          <tr>
            <th></th>
            <th >無氧銅線<br>OFC</th>
            <th >高導性無氧銅線<br>OFHC</th>
            <th >單方向結晶無氧銅線<br>PCVCC-02</th>
            <th >單方向結晶無氧銅線<br>PCVCC-03</th>
            <th >右邊為測試欄位 <br>test-number1</th>
            <th >SnCu0.6% <br>(C50100)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td >純度<br>( % )</td>
            <td >≧99.95</td>
            <td >≧99.95</td>
            <td >≧99.95</td>
            <td >≧99.95</td>
            <td >≧99.95</td>
            <td >≧99.95</td>        
          </tr>
          <tr>
            <td >含氧量<br>( ppm )</td>
            <td >≦15</td>
            <td ><1</td>
            <td ><1</td>
            <td ><1</td>
            <td ><1</td>
            <td ><1</td>        
          </tr>
          <tr>
            <td >導電率<br>( %IACS )</td>
            <td >>100</td>
            <td >>100</td>
            <td >>102</td>
            <td >>102</td>
            <td >>100</td>
            <td >>100</td>        
          </tr>
          <tr>
            <td >晶粒組織<br></td>
            <td ></td>
            <td ></td>
            <td ><img src="./src/dist/image/single-page/page-material-1.png" alt=""></td>
            <td ><img src="./src/dist/image/single-page/page-material-2.png" alt=""></td>
            <td ><img src="./src/dist/image/single-page/page-material-1.png" alt=""></td>
            <td ><img src="./src/dist/image/single-page/page-material-2.png" alt=""></td>        
          </tr>
        </tbody>
      </table>
    </div>
    <section class="page-material-bg" style="background-image: url('./src/dist/image/single-page/page-material-bg.jpg')">

    </section>
    <div class="d-flex ">
      <div class="sim-button button28 hotnews-btn-mt ">
        <a href="about-alloy.php">查看其它資訊</a>
      </div> 
    </div>  
  </section>
  
  
    <?php include './component/footer.php' ?>
<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>

  <!-- <section class="jumbotron-header container-wrapper">
    <video src="./src/dist/video/nizing_video4.mp4" autoplay muted playsinline loop poster="./src/dist/video/home_video_poster.png"></video>
  </section> -->
  <style>
     
   @media only screen and (min-width:577px) {
      .pc1-bg{
        background-image: url('./src/dist/image/1.jpg');
        padding-bottom:45%
      }
      .pc2-bg{
        background-image: url('./src/dist/image/2.jpg');
        padding-bottom:45%
      }
      #products-business-section .slider .col-sm-3:nth-of-type(4n+1),#products-info .slider .col-sm-3:nth-of-type(4n+1){
        padding-left:15px;
        padding-right: 7.5px 
      }
      #products-business-section .slider .col-sm-3:nth-of-type(4n+2),#products-info .slider .col-sm-3:nth-of-type(4n+2){
        padding-left:7.5px;
        padding-right: 7.5px 
      }
      #products-business-section .slider .col-sm-3:nth-of-type(4n+3),#products-info .slider .col-sm-3:nth-of-type(4n+3){
        padding-left:7.5px;
        padding-right: 7.5px 
      }
      #products-business-section .slider .col-sm-3:nth-of-type(4n+4),#products-info .slider .col-sm-3:nth-of-type(4n+4){
        padding-right:15px;
        padding-left: 7.5px 
      }
    }
    @media only screen and (max-width:576px) {
      .pc1-bg{
        height: 90vh;
        background-image: url('./src/dist/image/1-M1-1.jpg')!important;
      }
      .pc2-bg{
        height: 90vh;
        background-image: url('./src/dist/image/1-M2-1.jpg')!important;
      }
      #products-business-section .slider .col-6:nth-of-type(2n+1),#products-info .slider .col-6:nth-of-type(2n+1){
        padding-left: 15px;
        padding-right: 7.5px 
      }
      #products-business-section .slider .col-6:nth-of-type(2n+2),#products-info .slider .col-6:nth-of-type(2n+2){
        padding-right: 15px;
        padding-left: 7.5px 
      }
    }

@-webkit-keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  40% {
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
  }
  60% {
    -webkit-transform: translateY(-15px);
            transform: translateY(-15px);
  }
}
@keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  40% {
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
  }
  60% {
    -webkit-transform: translateY(-15px);
            transform: translateY(-15px);
  }
}
    .fa-circle {
  color: #666;
}

.arrow-down {
  color: white;
  position: absolute;
  bottom: 50px;
  left: 50%;
  transform: translateX(-50%);
}
.arrow-down .fa-stack {
  -webkit-animation: bounce 2s infinite;
          animation: bounce 2s infinite;
}
.arrow-down .fa-stack:hover {
  -webkit-animation: none;
          animation: none;
}
.jumbotron-header{
  position: relative;
}
.slider-item-img{
  padding-bottom: initial;
}
.slider-item-img img{
  position: initial;
}
.slider-item{
        margin-bottom: 20px!important;
      }
  </style>
  <section class="jumbotron-header container-wrapper">
   <div class="slider">
      <div  class="slider-item m-0">
        <a href="#">
          <div class="pc1-bg" style="background-size:cover " ></div>
        </a>
      </div>
      <div  class="slider-item m-0 ">
        <a href="#">
          <div class="pc2-bg" style="background-size:cover"></div>
        </a>
      </div>
    </div>
    <a href="javascript:;" id="slider-down" class="hide-on-palm arrow-down">
      <span class="fa-stack fa-2x">
        <i class="fa fa-circle fa-stack-1x"></i>
        <i class="fa fa-chevron-down fa-stack-1x" style="font-size:20px"></i>
      </span>
    </a>
  </section>
  <section class="wrapper-space products-infomation-section">
    <h2>產品資訊</h2>
    <div class="row slider m-0">
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic01.jpg" alt="">
            </div>
            <h3>PVC1系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic02.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic03.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic04.jpg" alt="">
            </div>
            <h3>PVC4系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic01.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic02.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic03.jpg" alt="">
            </div>
            <h3>PVC4系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic04.jpg" alt="">
            </div>
            <h3>PVC1系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic01.jpg" alt="">
            </div>
            <h3>PVC1系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic02.jpg" alt="">
            </div>
            <h3>PVC1系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic03.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic04.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
    </div>
    <!-- <div class="slider">
      <div class="slider-item">
        <a href="#">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic02.jpg" alt="">
          </div>
          <h3>PVC1系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic01.jpg" alt="">
          </div>
          <h3>PVC2系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic02.jpg" alt="">
          </div>
          <h3>PVC3系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic03.jpg" alt="">
          </div>
          <h3>PVC4系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic04.jpg" alt="">
          </div>
          <h3>PVC5系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic01.jpg" alt="">
          </div>
          <h3>PVC6系列</h3>
        </a>
      </div>
    </div> -->
    <!-- <div class="d-flex">
      <div class="sim-button button28">
        <a href="about-product.php">所有產品</a>
      </div>
    </div> -->
  </section> 
  <section class="wrapper-space products-business">
    <h2>應用產業</h2>
    <div class="row slider m-0">
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic001.jpg" alt="">
            </div>
            <h3>最新消息系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic002.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic003.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic004.jpg" alt="">
            </div>
            <h3>PVC4系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic002.jpg" alt="">
            </div>
            <h3>最新消息系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic004.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic001.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic003.jpg" alt="">
            </div>
            <h3>PVC4系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic002.jpg" alt="">
            </div>
            <h3>最新消息系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic001.jpg" alt="">
            </div>
            <h3>PVC2系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic004.jpg" alt="">
            </div>
            <h3>PVC3系列</h3>
          </a>
        </div>
      </div>
      <div class="col-sm-3 col-6">
        <div class="slider-item m-0">
          <a href="#">
            <div class="slider-item-img">
              <img src="./src/dist/image/applied-pic003.jpg" alt="">
            </div>
            <h3>PVC4系列</h3>
          </a>
        </div>
      </div>
    </div>
    <!-- <div class="slider">
      <div class="slider-item">
        <a href="#">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic001.jpg" alt="">
          </div>
          <h3>最新消息系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic002.jpg" alt="">
          </div>
          <h3>PVC2系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic003.jpg" alt="">
          </div>
          <h3>PVC3系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic004.jpg" alt="">
          </div>
          <h3>PVC4系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic005.jpg" alt="">
          </div>
          <h3>PVC5系列</h3>
        </a>
      </div>
      <div class="slider-item">
        <a href="">
          <div class="slider-item-img">
            <img src="./src/dist/image/applied-pic004.jpg" alt="">
          </div>
          <h3>PVC6系列</h3>
        </a>
      </div>
    </div> -->
    <!-- <div class="d-flex">
      <div class="sim-button button28">
        <a href="about-application.php">所有應用產品</a>
      </div>
    </div> -->
  </section> 
  <section class="wrapper-space hotnews-section pb-0" style="background-image: url('./src/dist/image/hotnews-bg.jpg')">
    <h2>最新消息</h2>
    <div class="container py-0">
      <div class="row">
        <a class="w-100" href="#">
          <div class="col-12 hotnews-item">
            <div class="hotnews-item-title">
              <div>
                <p>2018.04.20</p>
                <p>遷移啟事：本公司將於107/06/04(一)遷至新址，並照常出貨至06/01(五)，06/02(六)全廠加班搬遷，06/04(一)恢復正常營運，搬遷期間一切正常營運，屆時請依新址、電話及傳真聯絡，繼續給予支持指教！</p>
              </div>
            </div>
          </div>
        </a>
        <a class="w-100" href="#">
          <div class="col-12 hotnews-item">
            <div class="hotnews-item-title">
              <div>
                <p>2018.08.23</p>
                <p>一年一度的中元普度，保佑日進生意興隆~</p>
              </div>
            </div>
          </div>
        </a>
        <a class="w-100" href="#">
          <div class="col-12 hotnews-item">
            <div class="hotnews-item-title">
              <div>
                <p>2018.03.28</p>
                <p>4/4(三)~4/8(日)為清明連假，為配合貨運公司，4/3出貨將於4/9送達，請多加留意~</p>
              </div>
            </div>
          </div>
        </a>
        <a class="w-100" href="#">
          <div class="col-12 hotnews-item">
            <div class="hotnews-item-title">
              <div>
                <p>2016.11.17</p>
                <p>我司將於12/26~12/27進行年度盤點作業，最後出貨時間為12/23 17:00，並於12/28恢復正常作業。</p>
              </div>
            </div>
          </div>
        </a>
        <a class="w-100" href="#">
          <div class="col-12 hotnews-item">
            <div class="hotnews-item-title">
              <div>
                <p>2015.11.30</p>
                <p>我司將於12/25~12/27進行年度結帳相關作業，最後出貨時間為12/24 17:00，並於12/28恢復正常作業</p>
              </div>
            </div>
          </div>
        </a>      
      </div>
    </div>
    <div class="d-flex">
      <div class="sim-button button28 hotnews-btn-mt">
        <a href="#">全部新聞</a>
      </div> 
    </div>
  </section>

<?php 
  $json1 = '[
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-02.jpg",
      "title": "UL台灣官網"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-03.jpg",
      "title": "VDE官方網站"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-02.jpg",
      "title": "UL台灣官網"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-03.jpg",
      "title": "VDE官方網站"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-02.jpg",
      "title": "UL台灣官網"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-03.jpg",
      "title": "VDE官方網站"
    },
    {
      "link": "#",
      "img": "./src/dist/image/links-01.jpg",
      "title": "即時查詢銅價"
    }
  ]';
  $objs=json_decode($json1);
?>

  <div class="wrapper-space  hotnews-section commonly-associate" style="background-image: url('./src/dist/image/commonly-associate-bg.jpg')">
      <h2>常用連結</h2>
      <div class="index-three-item-wrapper">
        <div class="container">
          <div class="row">

            
            <?php
              foreach($objs as $obj):
             ?>

            <!-- repeat -->
            <div class="col-4">
              <div class="item">
                <a href="#">
                  <div class="item-img">
                    <img src="<?php echo $obj->{'img'};?>" alt="">
                  </div>
                  <h3 class="item-title"><?php echo $obj->{'title'};?></h3>
                </a>
              </div>
            </div>
            <!-- repeat -->
            
            <?php 
              endforeach;
            ?>
          </div>
        </div>
        <!-- <div class="cst-container p-0">
          <div class="slider">
            <div class="slider-item">
              <a href="#">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-01.jpg" alt="">
                </div>
                <h3>即時查詢銅價</h3>
              </a>
            </div>
            <div class="slider-item">
              <a href="">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-02.jpg" alt="">
                </div>
                <h3>UL台灣官網</h3>
              </a>
            </div>
            <div class="slider-item">
              <a href="">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-03.jpg" alt="">
                </div>
                <h3>VDE官方網站</h3>
              </a>
            </div>
            <div class="slider-item">
              <a href="">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-01.jpg" alt="">
                </div>
                <h3>即時查詢銅價</h3>
              </a>
            </div>
            <div class="slider-item">
              <a href="">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-02.jpg" alt="">
                </div>
                <h3>UL 台灣官網</h3>
              </a>
            </div>
            <div class="slider-item">
              <a href="">
                <div class="slider-item-img slider-iteml-2wh">
                  <img src="./src/dist/image/links-01.jpg" alt="">
                </div>
                <h3>即時查詢銅價</h3>
              </a>
            </div>
          </div>
        </div> -->
      </div>
    </div>

<?php 
  $json2 = '[
    "./src/dist/image/PRESTIGIOUS-01.jpg",
    "./src/dist/image/PRESTIGIOUS-02.jpg",
    "./src/dist/image/PRESTIGIOUS-03.jpg",
    "./src/dist/image/PRESTIGIOUS-04.jpg",
    "./src/dist/image/PRESTIGIOUS-05.jpg",
    "./src/dist/image/PRESTIGIOUS-02.jpg",
    "./src/dist/image/PRESTIGIOUS-03.jpg"
  ]';
  $objs2=json_decode($json2);
?>

  <section class="wrapper-space cooperative">
    <h2>合作企業</h2>
    <div class="index-three-item-wrapper">
      <div class="container">
        <div class="row">
          <?php
            foreach($objs2 as $obj):
          ?>
          <!-- repeat -->
          <div class="col-4 col-lg-3">
            <div class="item">
              <a href="#">
                <div class="item-img-bigger">
                  <img src="<?php echo $obj;?>" alt="">
                </div>
              </a>
            </div>
          </div>
          <!-- repeat -->
          
          <?php 
            endforeach;
          ?>

        </div>
      </div>
    </div>
    <!-- <div class="slider-wapper">
      <div class="cst-container p-0">
        <div class="slider">
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-01.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-02.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-03.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-04.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-05.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-02.jpg" alt="">
            </a>
          </div>
          <div class="slider-item slider-item-nm">
            <a href="">
              <img class="img-fluid" src="./src/dist/image/PRESTIGIOUS-03.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </div> -->
  </section>
  <?php include './component/footer.php' ?>
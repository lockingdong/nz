<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>

<style>
html, body {
  overflow-x: hidden !important;
}
</style>

<div class="item-inner-wrapper">
  <section class="container pb-10">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb p-0">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item"><a href="#">產品資訊</a></li>
        <li class="breadcrumb-item"><a href="#">高溫線系列</a></li>
        <li class="breadcrumb-item active">醫療用呼吸管加熱線</li>
      </ol>
    </nav>

    <?php 
      $json = '[
        "./src/dist/image/single-page/item-inner-pic-1.jpg",
        "./src/dist/image/single-page/item-inner-pic-2.jpg",
        "./src/dist/image/single-page/item-inner-pic-1.jpg",
        "./src/dist/image/single-page/item-inner-pic-2.jpg",
        "./src/dist/image/single-page/item-inner-pic-1.jpg"
      ]';
      $objs = json_decode($json);
      // echo var_dump($objs);
    ?>

    <section class="item-inner-content">
      <h1>醫療用呼吸管加熱線</h1>
      <a class="Download_item-pdf" href="" download=""><img src="./src/dist/image/single-page/PDF.svg" alt="PDF">下載PDF檔</a>
      <div class="slider Slick_item-banner">
        <div class="slider-item">
            <div class="slider-item-img">
              <img src="./src/dist/image/single-page/item-inner-pic-1.jpg" alt="">
            </div>
        </div>
        <div class="slider-item">
            <div class="slider-item-img">
              <img src="./src/dist/image/single-page/item-inner-pic-2.jpg" alt="">
            </div>
        </div>
        <div class="slider-item">
            <div class="slider-item-img">
              <img src="./src/dist/image/single-page/item-inner-pic-1.jpg" alt="">
            </div>
        </div>
        <div class="slider-item">
            <div class="slider-item-img">
              <img src="./src/dist/image/single-page/item-inner-pic-2.jpg" alt="">
            </div>
        </div>
      </div>
      
      <!-- <div class="container">
        <div class="row">
          <?php foreach($objs as $obj): ?>
          <div class="col-12 col-md-6 col-lg-3">
            <div class="img-wrap">
              <img src="<?php echo $obj; ?>" alt="">
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div> -->


      <div class="Slick_item-info">
       <h3 class="Slick_item-info-title Slick_item-info-title-range">應用範圍</h3>
       <p>UL3075電線適用於各種家用電器、照明燈具、工業機器、電熱製品、原料熔爐等高溫場所之配線。通過UL VW-1垂直燃燒測試以及多項IEC測試，證實為低煙、無鹵、耐燃之產品。</p>
      </div> 
    </section>
  </section >
</div>
<section class="container">
  <div class="Slick_item-info">
    <h3 class="Slick_item-info-title Slick_item-info-title-skill Slick_item-info-title-black">技術資料</h3>
    <p>額定電壓：	600V AC/750V DC</p>
    <p>溫度範圍：	-60°C~+200°C</p>
    <p>外徑容差：	±0.1mm</p>
    <p>試驗電壓：  2000V</p>
    <p>導體：	鍍錫銅線</p>
    <p>絕緣體：	PVC</p>
    <p>顏色：	白 黑 紅 黃 藍 綠 棕 黃滾綠</p>
    <h3 class="Slick_item-info-title Slick_item-info-title-pro Slick_item-info-title-black">專業認證</h3>
  </div>

  <!-- <div class="slider Slick_pro">
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/PSE.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/UL.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/VDE.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/ROHS.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/VDE.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/UL.jpg" alt="">
      </a>
    </div>
    <div class="slider-item slider-item-nm"> 
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/ROHS.jpg" alt="">
      </a>
  </div> -->

  <div class="row">
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/PSE.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/UL.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/VDE.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/ROHS.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/VDE.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3">
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/UL.jpg" alt="">
      </a>
    </div>
    <div class="col-6 col-sm-3"> 
      <a href="">
        <img class="img-fluid" src="./src/dist/image/single-page/ROHS.jpg" alt="">
      </a>
    </div>
  </div>
    
  </div>
  <div class="item-table-wrapper">
    <h3 class="Slick_item-info-title Slick_item-info-title-data Slick_item-info-title-black">產品規格資料</h3>
    <div class="">
      <table class="table table-even-odd ">
        <tbody>
          <tr class="table-title">
            <td colspan="10">GE 矽膠編織耐熱線 產品規格表</td>
          </tr>
          <tr class="table-item table-item-en">
            <td colspan="3">導體</td>
            <td colspan="3">絕緣體</td>
            <td rowspan="2">完成外徑</td>
            <td colspan="2">電氣特性</td>
            <td>包裝</td>
          </tr>
          <tr class="table-item table-item-en">
            <td>This's test word</td>
            <td>導體結構</td>
            <td>內徑</td>
            <td>絕緣厚度</td>
            <td>矽膠外俓</td>
            <td>玻璃纖維厚度</td>
            <td>導體電阻</td>        
            <td>電流量</td>        
            <td>米/卷</td>                
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
          <tr>
            <td>0.3(22AWG)</td>
            <td>12x0.18</td>
            <td>0.72</td>
            <td>0.35</td>
            <td>1.42</td>
            <td>0.16</td>
            <td>1.74</td>        
            <td>64.4</td>        
            <td>3.0</td>
            <td>500</td>          
          </tr>
        
        </tbody>
      </table>
    </div>
  </div>
 
  

  
  
</section>
  
   

<?php include './component/footer.php' ?>
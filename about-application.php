<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron"> 
    <h1 class="text-center">應用產業<span></span></h1>
  </section>
  <div class="about-product-breadcrumb ">
    <section class="container breadcrumb-container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 pb-20">
          <li class="breadcrumb-item"><a href="#">首頁</a></li>
          <li class="breadcrumb-item active">應用產業</li>
        </ol>
      </nav>
    </section> 
  </div>
  <div class="container about-application">
    <a class="row mb-10 about-page-bg-title about-application-bg-B" href="about-prod-item.php" >
      <div class="about-page-bg-info">
        <p class="about-content-title">車用元件</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a>
    <div class="row  mb-10">
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-02.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">溫控系統元件</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-03.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">機器設備</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
    </div> 
    <a class="row mb-10 about-page-bg-title about-application-bg-B" href="about-prod-item.php" >
      <div class="about-page-bg-info">
        <p class="about-content-title">雲端系統元件</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a>
    <div class="row  mb-10">
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-05.jpg')">
        <div class="about-page-bg-info ">
          <p class="about-content-title">太陽能元件</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-06.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">機器手臂</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
    </div> 
    <a class="row mb-10 about-page-bg-title about-application-bg-B" href="about-prod-item.php" >
      <div class="about-page-bg-info">
        <p class="about-content-title">加熱元件</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a>
    <div class="row  mb-10">
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-08.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">LED燈</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-09.jpg')">
        <div class="about-page-bg-info ">
          <p class="about-content-title">建築配線</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
    </div>
    <a class="row mb-10 about-page-bg-title about-application-bg-B" href="about-prod-item.php" >
      <div class="about-page-bg-info about-page-bg-info-black">
        <p class="about-content-title">醫療元件</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a>
    <div class="row  mb-10">
        <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2" href="about-prod-item.php" style="background-image: url('./src/dist/image/about/about-application-011.jpg')">
          <div class="about-page-bg-info ">
            <p class="about-content-title">其他應用</p>
            <p class="about-seemore">查看詳情 ></p>
          </div>
        </a>

    </div>
   
  </div>
<?php include './component/footer.php' ?>
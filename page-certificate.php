<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center"><span>安規認證 - </span>規格認證</h1>
  </section>
  <section class="container about-certificate-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item"><a href="#">安規認證</a></li>
        <li class="breadcrumb-item"><a href="#">規格認證</a></li>
        <li class="breadcrumb-item active" aria-current="page">PSE</li>
      </ol>
    </nav>
    <section class="about-certificate page-certificate">
      <div class="row">
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg">
          <a href="./src/dist/image/paper_big_1.jpg"  data-lightbox="image-1"><img src="./src/dist/image/paper1.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_2.jpg"  data-lightbox="image-2"><img src="./src/dist/image/paper2.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_3.jpg"  data-lightbox="image-3"><img src="./src/dist/image/paper3.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_4.jpg"  data-lightbox="image-4"><img src="./src/dist/image/paper4.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_5.jpg"  data-lightbox="image-5"><img src="./src/dist/image/paper5.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_6.jpg"  data-lightbox="image-6"><img src="./src/dist/image/paper6.jpg" alt=""></a>
        </div>
        <div class="col-xl-20 col-lg-3 col-md-4 col-6  lightboxImg ">
         <a href="./src/dist/image/paper_big_7.jpg"  data-lightbox="image-7"><img src="./src/dist/image/paper7.jpg" alt=""></a>
        </div>
      </div>
     </section>
  </section>
  
  <?php include './component/footer.php' ?>
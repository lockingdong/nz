<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>

  <style>
    html, body {
      overflow-x: hidden !important;
    }
  </style>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center">合金導體</h1>
  </section>
  <section class="container breadcrumb-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb p-0 mt-20 mb-20">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item active">合金導體</li>
      </ol>
    </nav>
  </section> 
  <div class="container about-alloy">
    <a class="row mb-10 about-page-bg-title about-alloy-bg" href="page-alloy.php" style="background-image: url('./src/dist/image/about/about-alloy-01.jpg')">
      <div class="about-page-bg-info">
        <p class="about-content-title">純銅</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a> 
    <a class="row mb-10 about-page-bg-title about-alloy-bg" href="page-alloy.php" style="background-image: url('./src/dist/image/about/about-alloy-02.jpg')">
      <div class="about-page-bg-info">
        <p class="about-content-title">銀銅合金</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a> 
    <a class="row mb-10 about-page-bg-title about-alloy-bg" href="page-alloy.php" style="background-image: url('./src/dist/image/about/about-alloy-03.jpg')">
      <div class="about-page-bg-info">
        <p class="about-content-title">錫銅合金</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a> 
    <a class="row mb-10 about-page-bg-title about-alloy-bg" href="page-alloy.php" style="background-image: url('./src/dist/image/about/about-alloy-04.jpg')">
      <div class="about-page-bg-info about-page-bg-info-bg about-page-bg-info-black">
        <p class="about-content-title">銀銅合金</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a> 
    <a class="row mb-10 about-page-bg-title about-alloy-bg" href="page-alloy.php" style="background-image: url('./src/dist/image/about/about-alloy-05.jpg')">
      <div class="about-page-bg-info">
        <p class="about-content-title">純銀及銀合金</p>
        <p class="about-seemore">查看詳情 ></p>
      </div>
    </a> 
   
  </div>
  <?php include './component/footer.php' ?>
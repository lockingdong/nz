<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron">
    <h1 class="text-center"><span>日進電線 - </span>核心文化</h1>
  </section>
  <section class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb px-0">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item"><a href="#">日進電線</a></li>
        <li class="breadcrumb-item active" aria-current="page">核心文化</li>
      </ol>
    </nav>
    <h2 class="page-title">核心文化</h2>
    <article>
      <p>日進電線股份有限公司創立於1983年，耐熱電線電纜起步，目前日進電線已是台灣電線電纜及特殊線材產業領導廠商，同時成功跨足綠能光電、成為國際化企業。</p>
      <br>
      <p>日進電線所生產的矽膠線、補償導線、PVC線、不銹鋼線材，廣泛運用於電力傳輸、電信網路、交通運輸、工業生產等基礎建設。旗下核心事業中，電線電纜事業包含矽膠耐熱電線、補償導線、PVC照射線等電線電纜。電力電纜與通信線纜產品線完整，深耕台灣電力和電信需求。</p>
      <br>
      <p>日進電線擁有完整的產品系列，通過數種國際安規認可產品。以最熱忱的服務態度，不斷精進品質，開發新產品，和客戶共同發展、共同成長。由于您持續的支持與愛護，以前瞻性的佈局策略追求企業創新成長，進行自主技術之研究，依據市場及客戶需求開發新產品與新業務。日進電線秉持一貫對品質的嚴謹要求，以及快速整合的服務，成為客戶的最佳伙伴，在兩岸經濟發展的重要里程中，扮演關鍵的參與和推動角色，未來日進電線將在卓越製造技術與多樣化客戶基礎下持續深耕，同時積極掌握產業新興發展機會，創造企業發展的新里程。</p>
    </article>
  </section>
  <div class="marquee">
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-01.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-02.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-03.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-04.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-05.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-01.jpg');"></div>
    </div>
    <div class="marquee-item">
      <div class="marquee-bg" style=" background-image: url('./src/dist/image/marquee/company-cul-02.jpg');"></div>
    </div>
  </div>
<?php include './component/footer.php' ?>
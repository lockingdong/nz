$(document).ready(function() {

	/*****************************************
	 Preload assets
	******************************************/
	function preLoadAssets(){

    // create queue
		var queue = new createjs.LoadQueue();
		var videosTarget = null;
 
		queue.on("complete", handleComplete, this);

		// create manifest for files to load
		queue.loadManifest([ 
			{id:'myVideo', src : './src/dist/video/nizing_video5.mp4',type : createjs.AbstractLoader.BINARY}
		]);

		// handle  & show progress
		queue.on("progress", function(evt){
		  var p = queue.progress * 100;
		  document.querySelector('.preload-counter').textContent = " "+Math.round(p)+"%";

		  $(".p-bar").css("width", ""+Math.round(p)+"%")
		});

		// insert after load
		function handleComplete() {

			// Insert Image
			var image = queue.getResult("myImage");
			$('.img-holder').append(image);

      // Insert Video
			var videosTarget = queue.getResult("myVideo");
			var $video = $('<video id="home-video" autoplay loop muted playsinline />');
			var $source = $('<source type="video/mp4"/>');
			var src = videosTarget;
			var blob = new Blob( [ src ], { type: "video/mp4" } );
			var urlCreator = window.URL || window.webkitURL;
			var objUrl = urlCreator.createObjectURL( blob );
			$source.attr('src', objUrl);
			$video.append($source);
			$('.video-container').prepend($video);
			$("#home-video").addClass("fadein");
			$('.counter-area').fadeOut();
		}
	}


	/*****************************************
	Init functions
	******************************************/
	function init(){
		preLoadAssets();
	}
	init();

}); // end ready
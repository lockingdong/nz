(function($){
  $('.special-button').click(function(){
    if($('.bottom').hasClass('active')){
      $(".st0").attr("class", "st0");
      $('.top').toggleClass("active");
      $('.bottom').toggleClass("active");
    }else{
      $(".st0").attr("class", "st0 active");
      $('.top').toggleClass("active");
      $('.bottom').toggleClass("active");
    }
  });
  // dropdowm menu 

 var $window_w = $(window).width();
 resizeNavbar($window_w);
 function resizeNavbar($w){
  var $navbar_h = $('header').height();
  $('.header-mt').css("margin-top",$navbar_h);
  if($w>1200){
   $(".dropdown").on("click", function (e) {
     return ;
   });
   $(".dropdown-menu .close").on('click',function(e){
     $('.dropdown-menu').hide();
   });
   $(".dropdown").on("mouseenter",function(e){
     $(this).children(".dropdown-menu").show()
   })
  }else{
    $(".dropdown").on("mouseenter",function(e){
     return false;
    })
  }
 }
 // resize
//  $(window).on("resize",function(e){
//   clearTimeout($window_w);
//   $window_w = setTimeout(function() { 
//     resizeNavbar($window_w);
//   }, 500);
//  })
$('.arrow-down').on("click",function(){
  console.log('aaa')
  $('html,body').animate({scrollTop:$('#products-info').offset().top - 100}, 500)
})
  // header-mt
  var $navbar_h = $('header').height();
  $('.header-mt').css("margin-top",$navbar_h);

  // lightbox 
  $('.lightboxImg a').on('click',addBlur);
 
  function addBlur(){
    console.log('a');
    console.log($('section'));
    $('section').addClass('blur');
    $('footer').addClass('blur');
    $('header').addClass('blur');
    $('#lightboxOverlay').on('click',removeBlur);
    $('#lightbox').on('click',removeBlur);
    $('.lb-close').on('click',removeBlur);
  }
  function removeBlur(){
    console.log('b');
    $('section').removeClass('blur');
    $('footer').removeClass('blur');
    $('header').removeClass('blur'); 
  }
  // 輪播
  // 產業資訊
  $('.jumbotron-header .slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,

    prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
    nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',  

  });
  // $('.products-infomation-section .slider').slick({
  //   slidesToShow: 2,
  //   slidesToScroll: 2,
  //   autoplay: true,
  //   autoplaySpeed: 5000,
  //   prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
  //   nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
  //   responsive: [
  //     {
  //       breakpoint: 576,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1,
  //       }
  //     }
  //   ]
  // });
  // 產業商用
  // $('.products-business .slider').slick({
  //   slidesToShow: 2,
  //   slidesToScroll: 2,
  //   autoplay: true,
  //   autoplaySpeed: 5000,
  //   prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
  //   nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
  //   responsive: [
  //     {
  //       breakpoint: 576,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1,
  //       }
  //     }
  //   ]
  // });
  // 最新消息 合作企業
  $('.cooperative .slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
    nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 1068,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  }) 
  // 常用連結
  $('.commonly-associate .slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
    nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 1068,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 476,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  })
     
  //跑圖輪播
  $('.marquee').slick({
    speed: 10000,
    autoplay: true,
    autoplaySpeed: 0,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    swipe:false,
    draggable:false,
    arrows:false,
    dots:false,
    pauseOnHover:false,
    touchMove:false,
    pauseOnFocus:false
    });
    
    //產品內頁上圖輪播
    $('.Slick_item-banner').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots:true,
      prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
      nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
    })
    $('.Slick_pro').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      prevArrow: '<div class="slider-arrow arrow-prev"><i class="fas fa-angle-left"></i></div>',
      nextArrow: '<div class="slider-arrow arrow-next"><i class="fas fa-angle-right"></i></div>',
      dots:true,
      responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 476,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    })
       

    //產品內集結頁字數限制
    function Text_substring_check($el){
      if($el == 'page-proditem-msg-en'){
        Text_substring('.page-proditem-item-en',60)
        Text_substring('.page-proditem-msg-en',80)
      }
      if($el = 'page-proditem-msg-zh'){
        Text_substring('.page-proditem-msg-zh',30)
      }
    }
    function Text_substring($el,$len){
      $($el).each(function(){
        let $Text_len = $(this).text().length;
        if($Text_len > $len){
         var $Text_sub = $(this).text().substring(0,$len-3)+'...';
         $(this).text($Text_sub); 
        }
      })
    }
    if($('.page-proditem-list').find('.page-proditem-msg-en').length !== 0){
      Text_substring_check('page-proditem-msg-en')
    }else if($('.page-proditem-list').find('.page-proditem-msg-zh').length !== 0){
      Text_substring_check('.page-proditem-msg-zh')
    }

   
})(jQuery)
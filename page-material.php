<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>
  <section class="jumbotron jumbotron-fluid  nizing-jumbotron"> 
    <h1 class="text-center">材料特性 - 塑膠</h1>
  </section>
  <section class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb pb-20">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item "><a href="#">材料特性</a></li>
        <li class="breadcrumb-item active">塑膠</li>
      </ol>
    </nav>
  </section> 
  <section class="container page-product page-material-section">
    <div class="page-proditem-info-area">
      
      <div class="page-proditem-info-title">
        <div class="page-proditem-info-title-area" >
            <p class="mb-0">TPE</p>
        </div>
      </div>
      <div class="page-proditem-info proditem-product-m">
        <div>一般為兩項材質所組成，主要由一硬質奈米區域(Nano-Domain)因相分離而分散於一軟質橡膠區域中。此區域主要由具有剛性(Rigid)及可饒性(Flexbie)之分子所構成，且兩者間鍵結通常為共價鍵(Covalent Bond)。因此 TPE 的使用溫度範圍一般位於玻璃轉移溫度(Tg)與熔點(Tm)之間。 另一類橡塑膠合膠 TPO 或交聯型 TPO(TPV)，上述之軟質區則通常由富有彈性之橡膠所組成並分散於塑膠硬質基材中(Matrix)中，兩區域為不同之高分子 所組成，與前述之共價鍵結有所不同。</div>
      </div>
    </div>
    <div class="page-proditem-info-area">
      <div class="page-proditem-info-title">
        <div class="page-proditem-info-title-area" >
            <p class="mb-0">
              聚醯亞胺<br>
              (KAPTON)
            </p>
        </div>
      </div>
      <div class="page-proditem-info proditem-product-m">
        <div>
            物理性質
            <br>
            熱固性聚醯亞胺具有優異的熱穩定性、耐化學腐蝕性和機械性能，通常為橘黃色。石墨或玻璃纖維增強的聚醯亞胺的抗彎強度可達到345 MPa,抗彎模量達到20GPa.熱固性聚醯亞胺蠕變很小，有較高的拉伸強度。聚醯亞胺的使用溫度範圍覆蓋較廣，從零下一百餘度到兩三百度。
            <br>  
            化學性質
            <br>
            聚醯亞胺化學性質穩定。聚醯亞胺不需要加入阻燃劑就可以阻止燃燒。一般的聚醯亞胺都抗化學溶劑如烴類、酯類、醚類、醇類和氟氯烷。它們也抗弱酸但不推薦在較強的鹼和無機酸環境中使用。某些聚醯亞胺如CP1和CORIN XLS是可溶於溶劑，這一性質有助於發展他們在噴塗和低溫交聯上的應用。
        </div>
      </div>
    </div>
    <div class="page-proditem-info-area">
      <div class="page-proditem-info-title">
        <div class="page-proditem-info-title-area">
            <p class="mb-0">
              不鏽鋼
              <br>
              (SUS304)
            </p>
        </div>
      </div>
      <div class="page-proditem-info proditem-product-m">
        <div>
            200系列：	鉻-鎳-錳奧氏體不鏽鋼 <br>
            300系列：	鉻-鎳奧氏體不鏽鋼 <br>
            型號301：	延展性好，用於成型產品。也可通過機械加工使其迅速硬化。焊接性好。抗磨性和疲勞強度優於304不鏽鋼，產品如：彈簧、鋼構、車輪蓋。 <br>
            型號302：	耐腐蝕性同304，由於含碳相對要高因而強度更好。 <br>
            型號303：	通過添加少量的硫、磷使其較304更易切削加工。 <br>
            型號304：	通用型號；即18/8不鏽鋼。產品如：耐蝕容器、餐具、家俱、欄杆、醫療器材。標準成分是18 %鉻加8 %鎳。為無磁性、當雜質含量高時,加工後偶爾會呈現弱磁性、此弱磁性只能使用熱處理的方式消除。屬於無法藉由熱處理方法來改變其金相組織結構的不鏽鋼。<br>
            型號304 L：	與304相同特性，但低碳故更耐蝕、易熱處理，但機械性較差適用焊接及不易熱處理之產品。<br>
            型號304 N：	與304相同特性，是一種含氮的不鏽鋼，加氮是為了提高鋼的強度。<br>
            型號309：	較之304有更好的耐溫性。<br>
            型號309 S：	具多量鉻、鎳，故耐熱、抗氧化性佳，產品如：熱交換器、鍋爐零組件、噴射引擎。<br>
            型號310 S：	含最多量鉻、鎳，故耐熱、抗氧化性最佳熱交換器、鍋爐零組件、電機設備。<br>
            型號316：	繼304之後，第二個得到最廣泛應用的鋼種，主要用於食品工業和外科手術器材，添加鉬元素使其獲得一種抗腐蝕的特殊結構。由於較之304其具有更好的抗氯化物腐蝕能力因而也作「船用鋼」來使用。SS316則通常用於核燃料回收裝置。18/10級不鏽鋼通常也符合這個應用級別。特用於化學、海邊等易腐蝕環境、船舶裝配、建材。<br>
            型號316 L：	低碳故更耐蝕、易熱處理，產品如：化學加工設備、核能發電機、冷凍劑儲槽。<br>
            型號321：	除了因為添加了鈦元素降低了材料焊縫鏽蝕的風險之外其他性能類似304，適於焊接釀酒設備、蒸氣管、航空零件。<br>
            型號347：	添加安定化元素鈮，適於焊接航空器具零件及化學設備。<br>
            400系列：	肥粒鐵和馬氏體不鏽鋼<br>
            型號408：	耐熱性好，弱抗腐蝕性，11 %的Cr，8 %的Ni。<br>
            型號409：	除了因為添加了鈦元素，最廉價的型號（英美），通常用作汽車排氣管，屬肥粒鐵不鏽鋼（鉻鋼），適於焊接，成本較低汽車排氣管、石油設備。<br>
            型號410：	馬氏體（高強度鉻鋼），耐磨性好，抗腐蝕性較差，適於幫浦。其化學成分含13 %鉻、0.15 %以下的碳及少量的其他元素合金。原料價格較便宜，具有磁性、可經由熱處理硬化。一般用途有軸承、醫療用具及刀具等。<br>
            型號416：	添加了硫改善了材料的加工性能。<br>
            型號420：	含較高碳、硬度、強度更高，刃具級馬氏體不鏽鋼，類似布氏不鏽鋼（Brearley's stainless steel，英國治金家Harry Brearley）這種最早的不鏽鋼，可以做的非常光亮，適於刀、彈簧、外科器具、剃刀切頭、閥。<br>
            型號430：	肥粒鐵不鏽鋼，裝飾用，具磁性，例如用於汽車飾品。良好的成型性，但耐溫性和抗腐蝕性要差，適於扣接件、餐具、家具用品。其標準化學成份為16～18 %鉻，含碳量低。此類不銹鋼具有磁性。<br>
            型號434：	含鉬故耐蝕性較430優良，適於餐具、雨刷、汽車裝璜。<br>
            型號440：	高強度刃具鋼，含碳稍高，經過適當的熱處理後可以獲得較高屈服強度，硬度可以達到58HRC，屬於最硬的不鏽鋼之列。最常見的應用例子就是「剃鬚刀片」。常用型號有三種：440A、440B、440C，另外還有440F（易加工型）。<br>
            500系列：	耐熱鉻合金鋼。<br>
            600系列：	馬氏體沉澱硬化不鏽鋼。<br>
            型號630：	最常用的沉澱硬化不鏽鋼型號，通常也叫17－4；17 % Cr，4 % Ni。  <br>
        </div>
      </div>
    </div>
    <div class="d-flex ">
      <div class="sim-button button28 hotnews-btn-mt">
        <a href="about-material.php">查看其它資訊</a>
      </div> 
    </div>  
  </section>
  <?php include './component/footer.php' ?>
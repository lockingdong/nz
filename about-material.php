<?php include './component/header.php' ?>
<?php include './component/navbar.php' ?>

  <section class="jumbotron jumbotron-fluid  nizing-jumbotron"> 
    <h1 class="text-center">材料特性</h1>
  </section>
  <section class="container breadcrumb-container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb mb-20">
        <li class="breadcrumb-item"><a href="#">首頁</a></li>
        <li class="breadcrumb-item active">材料特性</li>
      </ol>
    </nav>
  </section> 
  <div class="container about-material">
    <div class="row ">
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-material-bg-col-2 mb-10" href="page-material.php" style="background-image: url('./src/dist/image/about/about-material-1.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">鐵氟龍</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-material-bg-col-2 mb-10" href="page-material.php" style="background-image: url('./src/dist/image/about/about-material-2.jpg')">
        <div class="about-page-bg-info ">
          <p class="about-content-title">矽膠</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-material-bg-col-2 mb-10" href="page-material.php" style="background-image: url('./src/dist/image/about/about-material-3.jpg')">
        <div class="about-page-bg-info about-page-bg-info-black">
          <p class="about-content-title">溫控系統元件</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-material-bg-col-2 mb-10" href="page-material.php" style="background-image: url('./src/dist/image/about/about-material-4.jpg')">
        <div class="about-page-bg-info ">
          <p class="about-content-title">導體</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
      <a class="col-sm-6 col-12 about-page-bg-title about-page-bg-col-2 about-material-bg-col-2 mb-10" href="page-material.php" style="background-image: url('./src/dist/image/about/about-material-5.jpg')">
        <div class="about-page-bg-info ">
          <p class="about-content-title">纏繞編織材料</p>
          <p class="about-seemore">查看詳情 ></p>
        </div>
      </a>
    </div> 
  </div>
  <?php include './component/footer.php' ?>
